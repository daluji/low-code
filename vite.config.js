import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import AutoImport from "unplugin-auto-import/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import { VantResolver } from "@vant/auto-import-resolver";
import path from "path";

export default defineConfig({
  base: "./", //开发或生产环境服务的公共基础路径 配置引入相对路径
  server: {
    host: "0.0.0.0", //使用IP能访问
    open: true, // 启动时自动在浏览器中打开应用程序
    port: 3000, //访问端口号
  },

  // 需要用到的插件数组
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [VantResolver(), ElementPlusResolver()],
    }),
    // Components({
    //   resolvers: [VantResolver()],
    // }),
  ],
  
  resolve: {
    // 配置文件系统路径别名
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
});
